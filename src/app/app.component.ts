import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dismissible = true;
  alerts: any[] = [{
    type: 'success',

    timeout: 5000
  }];


  add(): void {
    this.alerts.push({
      type: 'info',

      timeout: 5000
    });
  }

  onClosed(dismissedAlert: any): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }




}


